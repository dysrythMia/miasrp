from flask import Flask, render_template
from dash import Dash
from jinja2 import Template
from dash.dependencies import Input, State, Output
import dash_core_components as dcc
import dash_html_components as html
import plotly.graph_objs as go
import pandas as pd

server = Flask(__name__)



@server.route("/")
def index():
    return render_template(
        'index.html', img='/static/img/home-bg.jpg', heading="Mia's SRP", subhead="Project Timeline")


@server.route("/about")
def about():
    return render_template(
        'about.html', img='/static/img/about-bg.jpg', heading='About', subhead='Abstract, Research Question, and Break Down')

@server.route("/analysis")
def data():
    return render_template(
        'analysis.html', img='/static/img/data-bg.jpg', heading='Analysis', subhead='Results and Discussion')

@server.route("/rawgraphs")
def vis():
    return render_template(
        'vis.html')
app = Dash(server=server, url_base_pathname='/dash')


df = pd.read_csv(
    'bubble.csv')


app.layout = html.Div([
    dcc.Graph(
        id='life-exp-vs-gdp',
        figure={
            'data': [
                go.Scatter(
                    x=df[df['time'] == i]['namenumber'],
                    y=df[df['time'] == i]['subject'],
                    text=df[df['time'] == i]['name'],
                    mode='markers',
                    opacity=0.7,
                    marker={
                        'size': df['value']*2,
                        'line': {'width': 0.5, 'color': 'white'}
                    },
                    name=i
                ) for i in df.time.unique()
            ],
            'layout': go.Layout(
                xaxis={'type': 'linear', 'title': 'Tax ID'},
                yaxis={'title': 'Subject Number'},
                margin={'l': 40, 'b': 40, 't': 10, 'r': 10},
                legend={'x': 0, 'y': 1},
                hovermode='closest'
            )
        }
    )
])

@server.route("/conclusion")
def conclusion():
    return render_template(
        'conclusion.html', img='/static/img/conclusion-bg.jpg', heading='Conclusion', subhead='')

@server.route("/contact")
def contact():
    return render_template (
        'contact.html')



if __name__ == '__main__':
    server.run(debug=True)
